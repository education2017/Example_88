package hr.ferit.bruno.example_88;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Zoric on 31.8.2017..
 */

public interface TvMazeAPI {
    String BASE_URL = "http://api.tvmaze.com/";
    @GET("search/shows")
    Call<List<ShowResult>> getTvShows(@Query("q") String tvShow);
}
