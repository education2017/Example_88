package hr.ferit.bruno.example_88;

import android.text.Html;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zoric on 1.9.2017..
 */

class TvShow {
    @SerializedName("name") private String mName;
    @SerializedName("summary") private String mSummary;
    @SerializedName("image") private Poster mPoster;

    public TvShow() {}

    public TvShow(String name, String summary, Poster poster) {
        mName = name;
        mSummary = summary;
        mPoster = poster;
    }

    public String getName() {
        return mName;
    }

    public String getSummary() {
        return mSummary;
    }

    public Poster getPoster() {
        return mPoster;
    }
}
