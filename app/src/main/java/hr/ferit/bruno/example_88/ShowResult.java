package hr.ferit.bruno.example_88;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zoric on 1.9.2017..
 */

public class ShowResult {
    @SerializedName("score") private double mScore;
    @SerializedName("show") private TvShow mTvShow;

    public ShowResult() {
    }

    public ShowResult(double score, TvShow tvShow) {
        mScore = score;
        mTvShow = tvShow;
    }

    public double getScore() {
        return mScore;
    }

    public TvShow getTvShow() {
        return mTvShow;
    }
}
