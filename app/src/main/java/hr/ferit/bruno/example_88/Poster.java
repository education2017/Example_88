package hr.ferit.bruno.example_88;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Zoric on 1.9.2017..
 */

class Poster {
    @SerializedName("original") private String mPosterURL;

    public Poster() {}

    public Poster(String posterURL) {
        mPosterURL = posterURL;
    }

    public String getPosterURL() { return mPosterURL; }
}
