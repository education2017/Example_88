package hr.ferit.bruno.example_88;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity implements Callback<List<ShowResult>> {

    private static final String ERROR_TAG = "ERROR";

    @BindView(R.id.etShowNameEntry) EditText etShowNameEntry;
    @BindView(R.id.ibSearch) ImageButton ibSearch;
    @BindView(R.id.lvTvShows) ListView lvTvShows;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.ibSearch)
    public void searchForTvShow(){
        String query = etShowNameEntry.getText().toString();
        loadTvShowInfo(query);
    }

    private void loadTvShowInfo(String tvShowName) {
        Retrofit retrofit = new  Retrofit.Builder()
                .client(new OkHttpClient())
                .baseUrl(TvMazeAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TvMazeAPI tvMazeAPI = retrofit.create(TvMazeAPI.class);
        Call<List<ShowResult>> request = (Call<List<ShowResult>>) tvMazeAPI.getTvShows(tvShowName);
        request.enqueue(this);
    }


    @Override
    public void onResponse(Call<List<ShowResult>> call, Response<List<ShowResult>> response) {
        List<ShowResult> showList = response.body();
        ShowAdapter adapter = new ShowAdapter(showList);
        lvTvShows.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<List<ShowResult>> call, Throwable t) {
        Log.e(ERROR_TAG, t.getMessage());
        Toast.makeText(this,R.string.API_ERROR,Toast.LENGTH_SHORT).show();
    }
}
