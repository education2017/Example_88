package hr.ferit.bruno.example_88;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zoric on 1.9.2017..
 */

class ShowAdapter extends BaseAdapter{

    List<ShowResult> mTvShows;

    public ShowAdapter(List<ShowResult> showList) {
        this.mTvShows = showList;
    }

    @Override
    public int getCount() {
        return this.mTvShows.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mTvShows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ShowViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_tvshow, parent, false);
            holder = new ShowViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ShowViewHolder) convertView.getTag();
        }

        TvShow show = this.mTvShows.get(position).getTvShow();

        holder.tvShowTitle.setText(show.getName());
        holder.tvShowSummary.setText(show.getSummary());
        Picasso.with(parent.getContext())
                .load(show.getPoster().getPosterURL())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_unavailable)
                .into(holder.ivShowThumbnail);

        return convertView;
    }

    static class ShowViewHolder{
        @BindView(R.id.ivShowThumbnail) ImageView ivShowThumbnail;
        @BindView(R.id.tvShowTitle) TextView tvShowTitle;
        @BindView(R.id.tvShowDescription) TextView tvShowSummary;

        public ShowViewHolder(View view){
            ButterKnife.bind(this,view);
        }
    }
}
